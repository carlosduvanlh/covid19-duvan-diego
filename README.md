![Datos Del Covid-19](http://www.madarme.co/portada-web.png)
# Título del proyecto: Covid-19

Este proyecto web trata de un reporte que contiene datos de personas relacionados con la enfermedad(Covid-19). Ejemplo (nombre, genero, sexo y estado).
## Tabla de contenido 

1. [Características](#características)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Académica](#institución-académica)

## Características

- Proyecto con lectura de datos JSON 
- Carga dinámica del JSON
- Archivo de JSON: [Click Aquí](https://raw.githubusercontent.com/dieguinduartee/jsoncovid/main/covid.json)

## Contenido del proyecto
[index.html](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/index.html)

- En este archivo hay 3 rutas (rf1-rf2-rf3), las cuales nos dirigen a los casos por departamento, municipio y una tabla en general sobre los casos.

[rf1.html](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/html/rf1.html)

- En este archivo se muestran los casos por departamento, el genero y una grafica con los casos.

[rf2.html](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/html/rf2.html)

- En este archivo se muestran los casos por municipio, si estan en estudio o relacionados, y una grafica.

[rf3.html](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/html/rf3.html)

- En este archivo se muestran los casos por departamentos, y una grafica de barras con los casos por departamentos.

[rf1.js](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/js/rf1.js)

- En este archivo js, se selecciona un departamento,  se grafica y muestra en una tabla sus casos usando como variable de análisis el  sexo de la persona.

[rf2.js](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/js/rf2.js)

- Se selecciona un municipio, se grafica y muestra en una tabla sus casos dependiendo de la fuente de contagio (Relacionado-Importado).

[rf3.js](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/js/rf3.js)

- Gráfica y muestra la tabla TOTAL, en donde se muestran los datos por departamento de casos positivos. 

[rf1.css](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/css/rf1.css),
[rf2.css](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/css/rf2.css),
[rf3.css](https://gitlab.com/carlosduvanlh/covid19-duvan-diego/-/blob/master/css/rf3.css)

- En estos archivos agregamos el estilo a nuestro proyecto web.

## Tecnologías

- HTML5
- JavaScript
- CSS3
- API GOOGLE CHARTS

## IDE

- El proyecto se desarrolla usando sublime text 3 
- Visor de JSON -(http://jsonviewer.stack.hu/)

## Instalación 
Firefox Developer Edition: [descargar](https://www.mozilla.org/es-ES/firefox/developer/) Software necesario para ver la interacción por consola y depuración del código JS.

- **Descargar** proyecto
- **Invocar** página index.html desde firefox

## Demo 

Para ver el demo de la aplicación puede dirigirse a: [Datos-Covid](http://ufps28.madarme.co/Datos-Covid/)

## Autor(es) 

Proyecto web desarrollado por:

- Carlos Duvan Labrador H. - carlosduvanlh@ufps.edu.co
- Diego Alexis Duarte M. - diegoalexisdm@ufps.edu.co

## Institución Académica 

Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]

   [Marco Adarme]: <http://madarme.co>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
