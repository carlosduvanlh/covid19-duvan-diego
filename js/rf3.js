window.onload = main;

async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {
        alert(err);
    }
}

function main() {
    let departamentos = {};
    leerJSON('https://raw.githubusercontent.com/dieguinduartee/jsoncovid/main/covid.json').then(data =>{
        data.map(x => {
            if (departamentos[x.departamento_nom]) {
                departamentos[x.departamento_nom]++;
            } else {
                departamentos[x.departamento_nom] = 1;
            }
        });
        let table = `
        <table border=1 align="center" >
            <tr>
                <th>Departamento</th>
                <th>Cantidad</th>
            </tr>
        `;
        for (const key in departamentos) {
            table += `
            <tr>
                <th>${key}</th>
                <th>${departamentos[key]}</th>
            </tr>
            `;
        }
        table+="</table>"
        document.getElementById("table").innerHTML = table;
        graficar(departamentos);
    })
}

function graficar(departamentos) {
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
    let lista = [['Departamento','Numero de Casos']]
    for (const key in departamentos) {
        lista.push([key,departamentos[key]]);
    }
    function drawChart() {
        var data = google.visualization.arrayToDataTable(lista);
        var options = {
            title: `Casos Covid Departamentos`,
            bar: {groupWidth: "75%"},
            legend: { position: "none" },
        };
        var chart = new google.visualization.BarChart(document.getElementById('Barchart'));
        chart.draw(data, options);
    }
}