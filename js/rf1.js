window.onload = main;

async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {
        alert(err);
    }
}

function main() {
    let departamentos = {};
    leerJSON('https://raw.githubusercontent.com/dieguinduartee/jsoncovid/main/covid.json').then(data =>{
        data.map(x => {
            if (departamentos[x.departamento_nom]) {
                departamentos[x.departamento_nom]++;
            } else {
                departamentos[x.departamento_nom] = 1;
            }
        });
        let html = document.getElementById("div");
        let select = "<select onchange='casosDTO()' id='DTO'>";
        for (const key in departamentos) {
            select += `<option>${key}</option>`;
        }
        
        select += "</select>"
        html.innerHTML = select;
    })
}

function casosDTO() {
    let DTO = document.getElementById("DTO").value;
    let mujeres = 0;
    let hombres = 0;
    leerJSON('https://martinmedinaruvian.github.io/json/covid19ColombiaMarzo.json').then(data =>{
        data.map(x => {
            if (x.departamento_nom == DTO && x.sexo == "F")
                mujeres++;
            if (x.departamento_nom == DTO && x.sexo == "M")
                hombres++;
        });
        let table = `
        <table border=1 align="center">
            <thead>
                <th>Mujeres</th>
                <th>Hombres</th>
            </thead>
            <tbody>
            <th>${mujeres}</th>
            <th>${hombres}</th> 
            </tbody>
        `
        document.getElementById("table").innerHTML = table;
        graficar(mujeres, hombres, DTO);
        // crearTabal(mujeres,hombres)
    })
}

function graficar(F, M, DTO) {
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Genero', 'Cantidad'],
            ['Mujeres', F],
            ['Hombres', M]
        ]);
        var options = {
            title: `Casos Covid Departamento ${DTO}`,
            colors: ['#c50084', '#0000ff']
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
}

// function crearTabal(F,M){
//     google.charts.load('current', {'packages':['table']});
//       google.charts.setOnLoadCallback(drawTable);

//       function drawTable() {
//         var data = new google.visualization.DataTable();
//         data.addColumn('string', 'Genero');
//         data.addColumn('number', 'cantidad');
//         data.addRows([
//           ['mujeres',F],
//           ['hombres',M],
//         ]);

//         var table = new google.visualization.Table(document.getElementById('table_div'));

//         table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
//       }
// }
